package com.devcamp.oddevennumberapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OddEvenNumberApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OddEvenNumberApiApplication.class, args);
	}

}
