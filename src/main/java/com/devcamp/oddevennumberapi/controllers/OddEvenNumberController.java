package com.devcamp.oddevennumberapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin
public class OddEvenNumberController {
    @GetMapping("/checknumber")
    public String checkNumberApi(@RequestParam( name = "number" , required = true) int Num){
        if (Num % 2 == 0){
            return "Đây là số chẵn (Odd) đó nha";
        }
        else return "Đây là số lẻ (Even) đó nha";
    }
}
